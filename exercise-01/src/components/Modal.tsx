import ReactDOM from 'react-dom'
import styles from './Modal.module.css'


interface ModalProps {
    onCancel: any
    style: any
    children: any
    dismissOnClickOutside: boolean
}

const Modal: React.FC<ModalProps> = ({
    dismissOnClickOutside,
    onCancel,
    style,
    children,
}) => {
    const modalRootEle = document.getElementById('modal-root')

    if (!modalRootEle) return (<> </>)

    return ReactDOM.createPortal(
        <div
            className={styles.modalContainer}
            onClick={(e) => {
                if (!dismissOnClickOutside) return

                const target: HTMLElement = e.target as HTMLElement
                if (target.parentElement === modalRootEle) {
                    onCancel()
                }
            }}
        >
            <div className="box" style={style}>
                {children}
            </div>
        </div>,
        modalRootEle
    )
}

export default Modal
