import { useState } from 'react'
import Modal from './Modal'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import { MembershipTierList } from '../store/data'

var dayjs = require('dayjs')

interface NewCustomerDialogProps {
    onCancelNewCustomer: any
    onAddCustomer: any
}

const NewCustomerDialog: React.FC<NewCustomerDialogProps> = ({
    onCancelNewCustomer,
    onAddCustomer,
}) => {
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [dob, setDob] = useState('')
    const [isActive, setActive] = useState<boolean>(false)
    const [tier, setTier] = useState('Platinum')
    const [expireDay, setExpireDay] = useState('')

    function handleDOB(text: string) {
        if (!text) return
        var selected = dayjs(text)
        setDob(selected)
    }

    function handleIsActive(yesNo: string) {
        setActive(yesNo === 'yes')
    }

    function handleTier(tier: string) {
        setTier(tier)
    }

    function handleExpireDate(date: string) {
        var expires = dayjs(date)
        setExpireDay(expires)
    }

    return (
        <Modal
            style={{ width: '50%', height: 'auto' }}
            dismissOnClickOutside={true}
            onCancel={onCancelNewCustomer}
        >
            <h2>Add Customer</h2>
            <div>
                <label>First Name:</label>
                <input
                    type="text"
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                />
                <br />
                <label>Last Name:</label>
                <input
                    type="text"
                    value={lastName}
                    onChange={(e) => setLastName(e.target.value)}
                />
                <br />
                {/* <label>DoB</label> */}
                {/* <input type = "date" id = 'dob' onSelect = {()=>{handleDOB()}}></input> */}
                <TextField
                    id="dob"
                    label="Birthday"
                    type="date"
                    defaultValue="2017-05-24"
                    onChange={(val) => {
                        handleDOB(val.target.value)
                    }}
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
                <br />
                <label>Is Active</label>
                <select
                    id="isActive"
                    onChange={(e) => {
                        handleIsActive(e.target.value)
                    }}
                >
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                <br />
                <label>Tier</label>
                <select
                    id="tier"
                    onChange={(e) => {
                        handleTier(e.target.value)
                    }}
                    defaultValue="Bronze"
                >
                    {MembershipTierList.map((tier, i) => {
                        return (
                            <option value={tier} key={`tier-${i}`}>
                                {tier}
                            </option>
                        )
                    })}
                </select>
                <br />
                <label>Expire day</label>
                <input
                    type="date"
                    id="expireDate"
                    onChange={(e) => {
                        handleExpireDate(e.target.value)
                    }}
                ></input>
            </div>
            <Button
                variant="contained"
                color="primary"
                onClick={() =>
                    onAddCustomer(
                        firstName,
                        lastName,
                        dob,
                        isActive,
                        tier,
                        expireDay
                    )
                }
            >
                Add
            </Button>
        </Modal>
    )
}

export default NewCustomerDialog
