import CustomerTable from './CustomerTable'
import { Switch, Route, useRouteMatch, useLocation } from 'react-router-dom'
import NewCustomerDialog from './NewCustomerDialog'

interface CustomersPageProps {
    onAddCustomer: (
        firstName: string,
        lastName: string,
        dob: any,
        isActive: boolean,
        tier: string,
        expireDay: any
    ) => void
    onCancelNewCustomer: () => void
}

const CustomersPage: React.FC<CustomersPageProps> = ({
    onAddCustomer,
    onCancelNewCustomer,
}) => {
    const routeMatch = useRouteMatch()
    const location = useLocation()

    const {path} = routeMatch

    return (
        <>
            <Switch>
                <Route path={`${path}/add`}>
                    <NewCustomerDialog
                        onAddCustomer={onAddCustomer}
                        onCancelNewCustomer={onCancelNewCustomer}
                    />
                </Route>
            </Switch>
            <main>
                <div className="box">
                    <CustomerTable />
                </div>
                <div>Route Match</div>
                <div>
                    {
                        JSON.stringify(routeMatch, null, 2)
                    }
                </div> 
                <div>Location</div>
                <div>
                    {
                        JSON.stringify(location, null, 2)
                    }
                </div> 
            </main>
        </>
    )
}

export default CustomersPage
