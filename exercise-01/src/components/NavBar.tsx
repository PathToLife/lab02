import styles from './NavBar.module.css'
import { NavLink } from 'react-router-dom'
import { useContext } from 'react'
import { AppContext } from '../store/app-context'

export default function NavBar() {
    //don't have to always have set
    const { customerData } = useContext(AppContext)

    var silverCount = 0
    var goldCount = 0
    var bronzeCount = 0
    var platinumCount = 0

    customerData.forEach((individual: any) => {
        // eslint-disable-next-line default-case
        switch (individual.membershipTier) {
            case 'Silver':
                silverCount++
                break

            case 'Bronze':
                bronzeCount++
                break

            case 'Gold':
                goldCount++
                break

            case 'Platinum':
                platinumCount++
                break
        }
    })
    return (
        <div className={styles.navBar}>
            <NavLink to="/customers" activeClassName={styles.activeLink}>
                Customers
            </NavLink>
            <div>
                Customer details: Bronze: {bronzeCount}, Silver: {silverCount},{' '}
                <br /> Gold: {goldCount}, <br /> Platinum: {platinumCount}.
                <br /> <strong>Total: </strong>
                {customerData?.length}
            </div>
        </div>
    )
}
