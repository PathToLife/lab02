import { Switch, Route, Redirect, useHistory } from 'react-router-dom'
import NavBar from './components/NavBar'
import CustomersPage from './components/CustomersPage'
import createPersistedState from 'use-persisted-state'
import { AppContext } from './store/app-context'
import { initialCustomers } from './store/data'

/**
 * Renders a navbar allowing the user to browse to the articles or gallery pages.
 * If the user tries to browse to any other URL, they are auto-redirected to the articles page.
 */

const useCustomerState = createPersistedState('data')

function App() {
    const history = useHistory()

    const [
        customerDataPersisted,
        setCustomerDataPersisted,
    ] = useCustomerState<any>(initialCustomers)

    function handleAddCustomer(
        firstName: string,
        lastName: string,
        dob: any,
        isActive: boolean,
        tier: string,
        expireDay: any
    ) {
        // const updatedCustoemers = [...customers]
        const updatedCustomers = [...customerDataPersisted]

        const newCustomer = {
            id: customerDataPersisted.length + 1,
            firstName: firstName,
            lastName: lastName,
            dob: dob,
            membershipTier: tier,
            membershipExpires: expireDay,
        }

        updatedCustomers.push(newCustomer)
        setCustomerDataPersisted(updatedCustomers)

        history.replace('/customers')
    }

    function handleCancelNewCustomer() {
        history.goBack()
    }

    return (
        <div className="container">
            <AppContext.Provider
                value={{
                    customerData: customerDataPersisted,
                    setCustomerData: (customerData: any) =>
                        setCustomerDataPersisted(customerData),
                }}
            >
                <nav>
                    <NavBar />
                </nav>

                <Switch>
                    <Route path="/customers">
                        <CustomersPage
                            onAddCustomer={handleAddCustomer}
                            onCancelNewCustomer={handleCancelNewCustomer}
                        />
                    </Route>

                    <Route path="*">
                        <Redirect to="/customers" />
                    </Route>
                </Switch>
            </AppContext.Provider>
        </div>
    )
}

export default App
