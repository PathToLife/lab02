import React from 'react'
import { ICustomers } from './data'

interface IAppContext {
    customerData: ICustomers[]
    setCustomerData: (customerData: any) => void
}

export const AppContext = React.createContext<IAppContext>({
    customerData: [],
    setCustomerData: () => {}
})
